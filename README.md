# Installation Guide:

- From Azure Portal create a new resource and choose custom template.
- Copy the content of azuredeploy.json in tmplate, and azuredeploy.parameters.json in parameters.
- Make sure to replace sentia-wp-we-test in all variables with your desired name for the project.
- Please follow the name convention:(Clientname-productname-region-environment-resourcetype).

# Afetr Installation do the following few configuration

- In the web application change Application General Settings the following
    1. PHP version: 7.2
    2. Platform: 64-bit
- In the web application under Deployment Slots add a new slot for 'staging'